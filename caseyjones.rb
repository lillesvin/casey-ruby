require 'json'
require 'rss'
require 'open-uri'
require 'slack-notifier'

class Casey
    attr_reader :config, :items

    def initialize(config_path)
        @config = JSON.parse(File.read(config_path), symbolize_names: true)
        @items  = get_items
    end

    def get_items
        open(@config[:fogbugz][:feedUrl]) do |rss|
            feed = RSS::Parser.parse(rss)
            return feed.items
        end
    end

    def print
        @items = get_items unless @items != nil
        @items.each do |item|
            puts " - %s (%s)" % [item.title, item.link]
        end
    end

    def build_slack_attachments
        @items = get_items unless @items != nil

        attachments = []

        items.each do |item|
            attachments << {
                fallback: "%s (%s)" % [item.title, item.link],
                text: "<%s|%s>" % [item.link, item.title],
                color: @config[:slack][:color],
                fields: [
                    {
                        title: "Category",
                        value: item.category.content,
                        short: true
                    },
                    {
                        title: "Opened",
                        value: item.pubDate,
                        short: true
                    }
                ]
            }
        end

        return attachments
    end

    def slack_it
        slack_opts = {
           channel: @config[:slack][:recipient],
           username: @config[:slack][:username],
        }

        notifier = Slack::Notifier.new(@config[:slack][:webhookUrl], slack_opts)
        notifier.ping(@config[:slack][:introText], attachments: build_slack_attachments)
    end
end

casey = Casey.new('config.json')
casey.slack_it

